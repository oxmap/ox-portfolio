import React, { Component } from 'react'

import './style.scss'

class Links extends Component {
  render() {
    return (
      <div className="links">
        <a href="https://www.dropbox.com/s/pe2feezius84svn/%D1%80%D0%B5%D0%B7%D1%8E%D0%BC%D0%B5.docx?dl=0" target="_blank">
          <div className="download">Скачать резюме</div>
        </a>
      </div>
    )
  }
}

export default Links