import React from 'react'
import Helmet from 'react-helmet'
import get from 'lodash/get'

import './style.scss'

import favicon16 from '../../assets/favicons/favicon-16x16.png'
import favicon32 from '../../assets/favicons/favicon-32x32.png'

class TemplateWrapper extends React.Component {
  render () {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const { children } = this.props

    return (
      <div className="template-wrapper">
        <Helmet
          title='Артем Савин'
          link={[
            { rel: 'icon', type: 'image/png', sizes: '16x16', href: favicon16 },
            { rel: 'icon', type: 'image/png', sizes: '32x32', href: favicon32 }
          ]}
        />
        <div className="template-wrapper-children">
          { children() }
        </div>
      </div>
    )
  }
}

export default TemplateWrapper
